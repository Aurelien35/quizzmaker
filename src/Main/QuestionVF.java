package Main;

    public class QuestionVF extends Question {
    public Boolean reponse;

    public  QuestionVF(String question, Boolean reponse){
        super (question, reponse.toString());
        this.reponse = reponse;
    }
    public String getText() {
        return this.question +System.lineSeparator()+ "Vrai ou Faux?";

    }
}