package Main;

public class QuestionQCM extends Question {
    public String choice;

    public QuestionQCM(String question, String reponse, String choice){
        super (question , reponse);
        this.choice = choice;
    }
    public String getText() {
        return this.question +System.lineSeparator()+ choice;

    }
}
