package Main;

import java.util.ArrayList;

public class Question {
    public String question;
    public String reponse;

    public Question(String question, String reponse) {
        this.question = question;
        this.reponse = reponse;


    }

    public String getText() {
        return this.question;
    }
    public Boolean tryAnswer(String answer){
        boolean isCorrect = answer.equals(this.reponse);
        return isCorrect;
    }
}