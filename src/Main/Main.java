package Main;

import org.w3c.dom.ls.LSOutput;

import javax.xml.namespace.QName;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Player player = new Player(); //Création du player
        ArrayList<Question> questions = new ArrayList(); // Création du tableau de question ouverte
        Question question1 = new Question("Quel est la couleur du cheval d'Henri IV?", "Blanc");  //Question
        Question question2 = new Question("Comment s'apelle le chien de Tintin?", "Milou");  //Question
        Question question3 = new Question("Comment s'apelle l'humain d'Idéfix?", "Obélix");   //Question
        questions.add(question1);  // Ajoute la question 1 au tableau de question ouverte
        questions.add(question2);  // Ajoute la question 2 au tableau de question ouverte
        questions.add(question3);  // Ajoute la question 3 au tableau de question ouverte
        QuestionQCM question4 = new QuestionQCM("Quel language suivant est \"Back-end?\"", "Java", "CSS - HTML - Java");   //Question QCM
        QuestionQCM question5 = new QuestionQCM("Dans quelle région est situé Rennes?", "Bretagne", "Bretagne - Normandie - Auvergne - Alsace");  // Question QCM
        QuestionQCM question6 = new QuestionQCM("Dans quelle département se situe Saint-Malo?", "Ille-et-Vilaine", "Mayenne - Paris - Finistère - Ille-et-Vilaine");  // Question QCM
        questions.add(question4);   // Ajoute la question 4 au tableau de question QCM
        questions.add(question5);   // Ajoute la question 5 au tableau de question QCM
        questions.add(question6);   // Ajoute la question 6 au tableau de question QCM
        ArrayList<QuestionQCM> questionsQCM = new ArrayList();   // Création du tableau de question QCM
        QuestionVF question7 = new QuestionVF("La Terre est ronde", true);  // Question Vrai/Faux
        QuestionVF question8 = new QuestionVF("Il y a 13 mois dans l'année", false); //Question Vrai/Faux
        QuestionVF question9 = new QuestionVF("Paris est la capitale de la France", true);  //Question Vrai/Faux
        questions.add(question7);   //Ajoute la question 7 au tableau de question Vrai/Faux
        questions.add(question8);   //Ajoute la question 8 au tableau de question Vrai/Faux
        questions.add(question9);   //Ajoute la question 9 au tableau de question Vrai/Faux
        ArrayList<QuestionVF> questionsVF = new ArrayList();   // Création du tableau de question QCM
        int count = 0;   //Initialisation du compteur

        for (                                    // Boucle pose de question + Intéraction avec l'utilisateur
               Question q : questions
             ) {
            System.out.println(q.getText());
            Scanner sc = new Scanner(System.in);
            String entry = sc.nextLine();

            if (q.tryAnswer(entry)) {                       //Condition de verification de réponse + incrémentation du compteur

                System.out.println("Bravo!!");
                count++;
            }
        }

            System.out.println("Vous avez "+count+" points!");    // Affichage du nombre de points


        if (count<2) {                               // Condition : Si le joueur a 0 ou 1 points ou si le joueur a plus de 1 point les message s'affichent

            System.out.println("Attention "+player.getName()+" ! Votre score n'est pas suffisant!!");}
        else if (count>1) {
            System.out.println("Bien joué "+player.getName()+" ! Vous avez brillé!!");}
    }


}

